if _G.__COPILOT_ENABLED == nil then
  _G.__COPILOT_ENABLED = true
end

local M = {}

function M.toggle()
  if _G.__COPILOT_ENABLED then
    vim.cmd [[ CopilotDisable ]]
    _G.__COPILOT_ENABLED = false
  else
    vim.cmd [[ CopilotEnable ]]
    _G.__COPILOT_ENABLED = true
  end
end

return M
