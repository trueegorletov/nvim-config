if _G.__GITUI_TERMINAL == nil then
  local Terminal = require("toggleterm.terminal").Terminal
  _G.__GITUI_TERMINAL = Terminal:new({ cmd = "gitui", hidden = true })
end

local M = {}

function M.toggle()
  _G.__GITUI_TERMINAL:toggle()
end

return M
