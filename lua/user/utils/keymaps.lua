local M = {}

-- Returns default options with merged opts
function M.default(opts)
	local def = {
		silent = true,
		noremap = true,
	}

	for k, v in pairs(opts or {}) do
		def[k] = v
	end

	return def
end

local avaliable_modes = { "n", "v", "x", "s", "o", "i", "l", "c", "t" }

function M.keymap(modes, lhs, rhs, opts)
	local mapfunc = type(rhs) == "function" and function(m, l, r, o)
		vim.keymap.set(m, l, r, o)
	end or function(m, l, r, o)
		vim.api.nvim_set_keymap(m, l, r, o)
	end

	opts = M.default(opts)
	opts.desc = opts.desc or tostring(rhs)

	for _, mode in ipairs(avaliable_modes) do
		if string.find(modes, mode) then
			mapfunc(mode, lhs, rhs, opts)
		end
	end
end

-- Shortcuts for non-usual options

function M.loud(opts)
	opts = opts or {}
	opts.silent = false
	return opts
end

function M.remap(opts)
	opts = opts or {}
	opts.noremap = false
	return opts
end

function M.expr(opts)
	opts = opts or {}
	opts.expr = true
	return opts
end

function M.script(opts)
	opts = opts or {}
	opts.script = true
	return opts
end

function M.desc(s)
	return {
		desc = s,
	}
end

function M.both(one, two)
	for key, value in pairs(two) do
		one[key] = value
	end

	return one
end

return M
