local status_ok, alpha = pcall(require, "alpha")
if not status_ok then
  return
end

local dashboard = require("alpha.themes.dashboard")

dashboard.section.header.val = {
  [[                           _         ]],
  [[      ___ _ __ _   _ _ __ | |_ ___   ]],
  [[ ====/ __| '__| | | | '_ \| __/ _ \  ]],
  [[ ===| (__| |  | |_| | |_) | || (_) | ]],
  [[ ====\___|_|   \__, | .__/ \__\___/  ]],
  [[ ~~~~~~~~~~~~~~|___/|_|~~~~~~~~~~~~  ]],
  [[ ~~~~~~~~~~~_~~~~~~~~~~~~~_~~~~~~~_  ]],
  [[ ~~~~~~~~~~| |~___ ~_~__~(_) __~_| | ]],
  [[  / __/ _ \| |/ _ \| '_ \| |/ _` | | ]],
  [[ | (_| (_) | | (_) | | | | | (_| | | ]],
  [[  \___\___/|_|\___/|_| |_|_|\__,_|_| ]],
  [[ ~~~~~~~~~~~~~~~~~~~~~~~~~~_~~~~~~~  ]],
  [[ ~~~~~~~~~~~~~~~~__~_~_~__| |_~___~  ]],
  [[ ===============/ _` | '__| __/ __|  ]],
  [[ ==============| (_| | |  | |_\__ \  ]],
  [[ ===============\__,_|_|   \__|___/  ]],
}

dashboard.section.header.opts.hl = "Type"

dashboard.section.buttons.val = {
  dashboard.button("s", " " .. " Restore last session", "<cmd>silent RestoreSession<cr>"),
  dashboard.button("r", " " .. " Recent files", "<cmd>Telescope oldfiles<cr>"),
  dashboard.button("e", " " .. " New file", "<cmd>ene <bar> startinsert<cr>"),
  dashboard.button("f", " " .. " Find file", "<cmd>Telescope find_files<cr>"),
  dashboard.button("p", " " .. " Find project", "<cmd>lua require('telescope').extensions.projects.projects()<cr>"),
  dashboard.button("t", " " .. " Find text", "<cmd>Telescope live_grep<cr>"),
  dashboard.button("c", " " .. " Config", "<cmd>e $MYVIMRC<cr>"),
  dashboard.button("q", " " .. " Quit", "<cmd>qa<cr>"),
}

local function footer()
  return "dktapps has brain gaps"
end

dashboard.section.footer.val = footer()

dashboard.section.footer.opts.hl = "Include"
dashboard.section.buttons.opts.hl = "Keyword"

dashboard.opts.opts.noautocmd = true

alpha.setup(dashboard.opts)
