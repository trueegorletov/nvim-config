local status_ok, surround = pcall(require, "nvim-surround")
if not status_ok then
	return
end

surround.setup({
  keymaps = {
    insert = "<m-g>s",
    insert_line = "<m-g>S",
    normal = "<leader>s",
    normal_cur = "<leader>ss",
    normal_line = "<leader>S",
    normal_cur_line = "<leader>SS",
    visual = "<leader>s",
    visual_line = "<leader>S",
    delete = "<leader>ds",
    change = "<leader>cs",
  }
})
