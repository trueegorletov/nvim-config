local utils = require("user.utils.keymaps")

-- Shorten function names
local keymap = utils.keymap
local desc = utils.desc
local expr = utils.expr
local script = utils.script
local both = utils.both

-- Modes
--   normal_mode = "n",
--   insert_mode = "i",
--   visual_mode = "v",
--   visual_block_mode = "x",
--   term_mode = "t",
--   command_mode = "c",

-- Better jumping to ends end starts
keymap("nvx", "L", "$", desc("Go to end of line"))
keymap("nvx", "H", "^", desc("Go to begin of line"))
keymap("nvx", "$", "L", desc("Go to end of screen"))
keymap("nvx", "^", "H", desc("Go to begin of screen"))

-- Better window navigation
keymap("n", "<c-h>", "<c-w>h", desc("Go to left split"))
keymap("n", "<c-j>", "<c-w>j", desc("Go to down split"))
keymap("n", "<c-k>", "<c-w>k", desc("Go to up split"))
keymap("n", "<c-l>", "<c-w>l", desc("Go to right split"))

-- Resize with arrows
keymap("n", "<c-up>", "<cmd>resize +2<cr>", desc("Resize upward"))
keymap("n", "<c-down>", "<cmd>resize -2<cr>", desc("Resize downward"))
keymap("n", "<c-left>", "<cmd>vertical resize +2<cr>", desc("Resize leftward"))
keymap("n", "<c-right>", "<cmd>vertical resize -2<cr>", desc("Resize rightward"))

-- Navigate buffers
keymap("n", "gt", "<cmd>BufferLineCycleNext<cr>", desc("Go to next buffer"))
keymap("n", "<m-l>", "<cmd>BufferLineCycleNext<cr>", desc("Go to next buffer"))
keymap("n", "gT", "<cmd>BufferLineCyclePrev<cr>", desc("Go to previous buffer"))
keymap("n", "<m-h>", "<cmd>BufferLineCyclePrev<cr>", desc("Go to previous buffer"))

-- Clear highlights
keymap("n", "<esc>", "<cmd>nohlsearch<cr>", desc("Clear highlights"))

-- Close buffers
keymap("n", "Q", "<cmd>Bdelete!<cr>", desc("Close current buffer"))

-- Better paste
keymap("v", "p", '"_dP', desc("Paste"))

keymap("nv", "<leader>p", '"*p', desc("Paste from system's clipboard"))
keymap("n", "<leader>P", '"*P', desc("Paste from system's clipboard (before cursor)"))
keymap("nv", "<leader>y", '"*y', desc("Yank to system's clipboard"))

-- Easier save
keymap("vnx", "<space>ww", "<cmd>w<cr>", desc("Save current buffer"))
keymap("vnx", "<space>wa", "<cmd>wa<cr>", desc("Save all buffers"))

-- Stay in indent mode
keymap("v", "<", "<gv", desc("Remove indent"))
keymap("v", ">", ">gv", desc("Add indent"))

-- Easier indent
keymap("n", "<", "v<", desc("Remove indent"))
keymap("n", ">", "v>", desc("Add indent"))

-- Plugins --

-- NvimTree
keymap("n", "<c-n>", "<cmd>NvimTreeToggle<cr>", desc("Toggle NvimTree"))
keymap("n", "<space>e", "<cmd>NvimTreeFocus<cr>", desc("Focus NvimTree"))

-- Telescope
keymap("n", "<space><space>", "<cmd>Telescope find_files<cr>", desc("Find files"))
keymap("n", "<space>ft", "<cmd>Telescope live_grep<cr>", desc("Live grep"))
keymap("n", "<space>fr", "<cmd>Telescope oldfiles<cr>", desc("Recent files"))
keymap("n", "<space>fp", "<cmd>Telescope projects<cr>", desc("Projects"))
keymap("n", "<space>fb", "<cmd>Telescope buffers<cr>", desc("Buffers"))

-- GitHub Copilot
local copilot = require("user.utils.copilot")

keymap("n", "<space>gc", function()
	copilot.toggle()
end, desc("Toggle GitHub Copilot enabled"))

keymap("i", "<c-g>", 'copilot#Accept("")', both(desc("Accept GitHub Copilot suggestion"), script(expr())))

-- Git
local gitui = require("user.utils.gitui")

keymap("nvxt", "<space>gg", gitui.toggle, desc("Toggle the gitui terminal"))

-- Comment
-- local comment = require("Comment.api")
--
-- keymap("n", "gcc", comment.toggle.linewise.current, desc("Toggle line comment"))
-- keymap("x", "gc", function ()
--   comment.toggle.linewise(vim.fn.visualmode())
-- end, desc("Toggle lines comment"))

-- DAP
local dap = require("dap")
local dapui = require("dapui")

keymap("n", "<space>db", dap.toggle_breakpoint, desc("Toggle breakpoint (DAP)"))
keymap("n", "<space>dc", dap.continue, desc("Continue (DAP)"))
keymap("n", "<space>di", dap.step_into, desc("Step into (DAP)"))
keymap("n", "<space>do", dap.step_over, desc("Step over (DAP)"))
keymap("n", "<space>dO", dap.step_out, desc("Step out (DAP)"))
keymap("n", "<space>dr", dap.repl.toggle, desc("Toggle repl (DAP)"))
keymap("n", "<space>dl", dap.run_last, desc("Run last (DAP)"))
keymap("n", "<space>du", dapui.toggle, desc("Toggle UI (DAP)"))
keymap("n", "<space>dt", dap.terminate, desc("Terminate (DAP)"))

-- Lsp
keymap("n", "<space>lf", function()
	vim.lsp.buf.format({ async = true })
end, desc("Run LSP format"))

-- Hop
keymap("nvx", "<leader>w", "<cmd>HopWordMW<cr>", desc("Jump to word"))
keymap("nvx", "<leader>l", "<cmd>HopLineMW<cr>", desc("Jump to line"))
keymap("nvx", "<leader>2", "<cmd>HopChar2MW<cr>", desc("Jump to bigram"))
keymap("nvx", "<leader>a", "<cmd>HopAnywhereMW<cr>", desc("Jump to anywhere"))

-- Ufo
local ufo = require("ufo")

keymap("n", "K", function()
	local winid = ufo.peekFoldedLinesUnderCursor()
	if not winid then
		-- nvimlsp
		vim.lsp.buf.hover()
	end
end, desc("Peek folded lines under cursor"))

keymap("n", "zR", ufo.openAllFolds, desc("Open all folds"))
keymap("n", "zM", ufo.closeAllFolds, desc("Close all folds"))

-- Utils
keymap("n", "<space>PS", "<cmd>PackerSync<cr>", desc("Run PackerSync"))
